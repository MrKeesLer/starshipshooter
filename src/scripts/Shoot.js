import Mobile from "./Mobile";

import imgSource from "../assets/images/tir.png"

export default class Shoot extends Mobile{

    constructor(x,y,w = 32, h = 8, hs = 8, vs = 0){
        super(x,y,imgSource,w,h,hs,vs);
    }

    move(canvas) {
        this.x = Math.min(canvas.width - this.width, this.x + this.speedH);
    }

    isCollidingWith(mobile){
        const xp1 = Math.max(this.x, mobile.x);
        const xp2 = Math.min(this.x + this.width, mobile.x + mobile.width);
        const yp1 = Math.max(this.y, mobile.y);
        const yp2 = Math.min(this.y + this.height, mobile.y + mobile.height);
        return xp1 < xp2 && yp1 < yp2;
    }

    checkCollide(saucers){
        let res = null;
        saucers.forEach(s => {
            if(this.isCollidingWith(s) && s.state == 0){  
                res = s;
            }
        });
        return res;
    }
}