export default class Mobile {

    constructor(x,y,imgSource,width,height,speedH,speedV){
        this.x = x;
        this.y = y;
        this.img = new Image(width,height);
        this.img.src = imgSource;
        this.speedV = speedV;
        this.speedH = speedH;
        this.width = width;
        this.height = height;
    }

    draw(context) {
        context.drawImage(this.img,this.x,this.y);
    }

    move(canvas){
        this.x += this.speedH;
        this.y += this.speedV;
    }

}