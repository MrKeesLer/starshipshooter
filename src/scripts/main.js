import Game from "./Game";
const theGame = new Game(document.getElementById("stars"),document.getElementById("score"));
const addButton = document.getElementById('nouvelleSoucoupe');
const waveButton = document.getElementById('flotteSoucoupes');
let waveActivate = 0;
// mise en place de l'action des clics sur les boutons + les gestionnaires du clavier pour contrôler le starship
const init = () => {
    window.addEventListener('keydown',theGame.controllShip.bind(theGame));
    window.addEventListener('keyup',theGame.stopControllShip.bind(theGame));

}

window.addEventListener("load",init);
addButton.addEventListener("click",addSaucer);
waveButton.addEventListener("click",() => {
    if(waveActivate == 0){
        waveActivate = window.setInterval(addSaucer, 750);
    } else {
        window.clearInterval(waveActivate);
        waveActivate = 0;
    }
    waveButton.blur();
});
//
console.log('le bundle a été généré');

function addSaucer(){
    theGame.addSaucer();
    addButton.blur();
}
